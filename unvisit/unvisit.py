import re
def findURL(strUrl):
	if (re.search('http://(?:.*\.)*dn\.se(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*aftonbladet\.se(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*idg\.se(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*expressen\.se(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*vg\.no(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*db\.no(?:/|$)*', strUrl)) or (re.search('http://(?:.*\.)*dagbladet\.no(?:/|$)*', strUrl)):
		m = re.search('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', strUrl)
		if m:
			return "http://unvis.it/"+m.group(0).replace("http://","").replace("https://","")+""
