import datetime

salaryDay = 25
today = datetime.datetime.today()
def strfdelta(tdelta, fmt):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return fmt.format(**d)

def SalDay(salYear, salMonth, salDay):
    swedishHolidays = [datetime.datetime(salYear, 6, 6),
                       datetime.datetime(salYear, 1, 1),
                       datetime.datetime(salYear, 12, 25),
                       datetime.datetime(salYear, 12, 26)]

    if datetime.datetime(salYear, salMonth, salDay, 0, 0).weekday() >= 5:
        if datetime.datetime(salYear, salMonth, salDay, 0, 0).weekday() == 6:
            return SalDay(salYear, salMonth, salDay - 2)
        else:
            return SalDay(salYear, salMonth, salDay - 1)
    elif (datetime.datetime(salYear, salMonth, salDay) in swedishHolidays):
        return SalDay(salYear, salMonth, salaryDay - 1)
    elif (today.day >= salaryDay):
        salMonth += 1
        if (salMonth == 13):
            salYear += 1
            salMonth = 1
            return SalDay(salYear, salMonth, salDay)
        else:
            return [salYear, salMonth, salDay]
    else:
        return [salYear, salMonth, salDay]

def get():
    today = datetime.datetime.today()
    salaryYear, salaryMonth, salDay = SalDay(today.year, today.month, salaryDay)
    salary = datetime.datetime(salaryYear, salaryMonth, salDay, 0, 0)
    countdown = (salary - today)
    return (strfdelta(countdown, "{days} dagar {hours} timmar {minutes} minuter"))

