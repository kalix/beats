# -*- coding: utf-8 -*-
import hashlib
import re

def registerCitizen(nick, account):
    if re.search('^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$',account):
        f = open('citizens.db', 'a')
        secretKey = hash('kalix'+nick+'1337') & 0xffffffff
        f.write("\n0 %s %s %s" % (nick, account, secretKey))
        return "Ditt personnummer i #kalix är %s" % secretKey
    else:
        return "Ange ett kontonummer, gör om gör rätt!"

def changeAccount(nick, secretKey, account):
    if (hash('kalix'+nick+'1337') & 0xffffffff) == int(secretKey):
        return "Inte implementerat än"
    else:
        return 0
