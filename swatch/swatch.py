import math
from datetime import datetime
from time import localtime, timezone
import re
import string

def findBeats(strMsg):
    if re.search('@+[0-9]+ ', strMsg):
        mtime = re.search('@+[0-9]+ ', strMsg).group(0)
        return mtime.replace("@","").replace(" ","")
def time2beat(time = 0):
    if not time: 
        h, m, s = localtime()[3:6]
    else:
        format = '%H:%M'
        times = datetime.strptime(time, format)
        h = times.hour
        m = times.minute
        s = 0
    beats = ((h * 3600) + (m * 60) + s + timezone) / 86.4

    if beats > 1000:
       beats -= 1000
    elif beats < 0:
       beats += 1000

    beats = "@%03d.beats" % (math.floor(beats))
    return beats

def beat2time(beats):
    beats = int(beats)
    totSeconds = 86.4*beats
    hours = int(totSeconds/3600)
    minutesDec = ((totSeconds - 3600*hours))/60
    minutes = int(minutesDec)
    seconds = int((minutesDec-minutes)*60)
    if beats >= 0 and beats  <= 1000:
        return "%02d:%02d:%02d" % (hours, minutes, seconds)


